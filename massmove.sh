#!/bin/bash
###########################################
# This script is for moving mass amounts of files that were left in an unorganized fashion.
# It takes a csv where the filename alone is in the first column and the path that it should exist in is the second.
# The first column isn't used in this script as the source filename to look for is also in the destination (column 2)
# I sort of wrote this like an arduino program....cause I'm into that these days.
###########################################

#This is the csv that's being passed in
INFILE=${1}
#Prefix to where the files are coming from (the search base)
SOURCE='/tmp/source'
#Prefix to where they are goin
DEST='/tmp/destination'
ERRORLOG=scripterror.log

#Used for generating test data
if [ "$(hostname -s)" == "AWS-CLI" ]; then
if [ "${2}" == "-r" ]; then
  find ${DEST} -type f -print | xargs -I % mv % ${SOURCE}
  rm scripterror.log
  rm -rf ${DEST}/*
  for i in $(head -n 1000 ${1} | cut -d, -f1 | tr '\\' '/'); do
    touch ${SOURCE}/$(basename $i); done
  exit
fi
fi
#Number of threads to run.
MAXTHREADS=5

MoveFile(){
(
  pathInDB=${1}
  destd="$(dirname ${pathInDB})"
  filename="$(basename ${1})"
  # Look for exiting file at destination
  if [ ! -f ${DEST}${pathInDB} ]; then
    if [ ! -d ${DEST}/${destd} ]; then
      mkdir -p  ${DEST}/${destd}
    fi
    # Look for destination folder and create if it doesn't already exist
#    origp=$(find ${SOURCE} -name ${filename} -type f -print)
    origp=${SOURCE}/${filename}
#    if [ ! -z "${origp}" ]; then
    if [ -f "${origp}" ]; then
      mv ${origp} ${DEST}${pathInDB} && \
        echo -e "Moved ${origp} to ${DEST}${pathInDB}" || \
        echo -e "ERROR Cannot move the file!"
    else
      echo -e "ERROR ${filename} wasn't found, moving on"
    fi
  else
    echo "WARNING ${DEST}${pathInDB} alread exists, skipping." 
  fi
  ) | tee -a scripterror.log
}

# main loop
while read -r data; do
  array=( $(echo ${data} | cut -d, -f 1 | tr '\\' '/'))
  while [ $(jobs -r | wc -l) -ge ${MAXTHREADS} ]; do
	sleep 1
  done
  MoveFile ${array[0]} ${array[1]} &
done < ${INFILE}
