#!/bin/bash
THREADS=15


createthread () {
  sleep $(  shuf -i 5-30 -n 1 )
}

while true; do
  #Count only RUNNING jobs
  while [ $(jobs -r | wc -l) -ge ${THREADS} ] ; do
    sleep 1
  done
  createthread &

  clear
  echo "command: jobs"
  jobs
  echo ================================
  echo "command: jobs -r"
  jobs -r
  echo ================================
done

